# README #

Manuscript files and data for:

## Description and spatial inference of soil drainage using matrix soil colours in the Lower Hunter Valley, New South Wales, Australia.

### Authors:

* Brendan Malone
* Alex McBratney
* Budiman Minasny

#### Submitted to PeerJ Jan 2018 (currently in review)


### Organisation of repository

### manuscript folder
Folder where published manuscript is place

### data

* 'reference_colors.txt': Reference soil colors as used in manuscript with regards Munsell soil colour observations and corresponding colour notations in other colour spaces including CIELAB.

* 'experimentalData_allObs.txt': Collated soil profile data with observed depths, colors and calcuated drainage index.




